const chai = require('chai')
const chaiHttp = require('chai-http')
const { stringify } = require('./../../helpers/str')
const app = require('./../../app')()
const { WorkspaceRepositoryInMemory, WorkspaceMemberRepositoryInMemory } = require('./../repository')
const { Workspace, WorkspaceMember } = require('./../domain')

let workspace1 = Workspace.createWorkspace('Gojek', 'gojek')
let workspace2 = Workspace.createWorkspace('Traveloka', 'traveloka')

let workspaceRepo = WorkspaceRepositoryInMemory.init()
let workspaceMemberRepo = WorkspaceMemberRepositoryInMemory.init()
workspaceRepo.Save(workspace1)
workspaceRepo.Save(workspace2)

workspaceMemberRepo.Save(WorkspaceMember.createWorkspaceMember('user-uid', workspace1.UID, 'admin'))

// Fake login
app.post('/auth/signin', (req, res) => {
  req.session.user_uid = 'user-uid'
  return res.json({ uid: req.session.user_uid })
})
app.use('/workspaces', require('./../../middleware/auth'), require('./workspace_server')(workspaceRepo, workspaceMemberRepo))

chai.use(chaiHttp)

let agent = chai.request.agent(app)

describe('Workspace http service test', () => {

  before(async() => await agent.post('/auth/signin'))

  after(() => agent.close())

  it('should get all workspace data', (done) => {
    agent.get('/workspaces')
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(res.body).to.deep.eql({
          data: [ stringify(workspace1) ]
        })
        done()
      })
  })

  it('can create workspace', (done) => {
    agent.post('/workspaces')
      .send({
        name: 'Bukalapak',
        slug: 'bukalapak'
      })
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(res.body.data).to.be.include({ name: 'Bukalapak', slug: 'bukalapak' })

        chai.expect(workspaceRepo.FindAll()).to.have.length(3)

        chai.expect(workspaceMemberRepo.FindAll()).to.have.length(2)
        chai.expect(workspaceMemberRepo.FindAll()[1])
          .to.be.include({ user_uid: 'user-uid', role: 'admin' })
        done()
      })
  })

  it('can fetch workspace by UID', (done) => {
    agent.get('/workspaces/' + workspace1.UID)
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(res.body.data).to.be.include({ name: 'Gojek', slug: 'gojek' })
        done()
      })
  })

  it('can fetch other workspace UID', (done) => {
    agent.get('/workspaces/' + workspace2.UID)
      .end((err, res) => {
        chai.expect(res).to.have.status(400)
        chai.expect(res.body.error).to.equal('Server error, resources is not found')
        done()
      })
  })

  it('can update workspace', (done) => {
    agent.put('/workspaces/' + workspace1.UID)
      .send({
        name: 'Gojek 1'
      })
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(res.body.data).to.be.include({ name: 'Gojek 1', slug: 'gojek' })
        done()
      })
  })

  it('can update workspace, because the workspace is not belonging to current user', (done) => {
    agent.put('/workspaces/' + workspace2.UID)
      .send({
        name: 'Gojek 1'
      })
      .end((err, res) => {
        chai.expect(res).to.have.status(400)
        chai.expect(res.body.error).to.equal('Server error, resources is not found')
        done()
      })
  })


  it('can not delete workspace, because the workspace is not belonging to current user', (done) => {
    let [ _workspace1, _workspace2, _workspace3 ] = workspaceRepo.FindAll()

    agent.delete('/workspaces/' + _workspace2.UID)
      .end((err, res) => {
        chai.expect(res).to.have.status(400)
        chai.expect(workspaceRepo.FindAll()).to.eql([ _workspace1, _workspace2, _workspace3 ])
        done()
      })
  })

  it('can delete workspace', (done) => {
    let [ _workspace1, _workspace2, _workspace3 ] = workspaceRepo.FindAll()

    agent.delete('/workspaces/' + _workspace1.UID)
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(workspaceRepo.FindAll()).to.eql([ _workspace2, _workspace3 ])
        done()
      })
  })

})
