const express = require('express')
const { Workspace, WorkspaceMember } = require('./../domain')
const { Error, ServerErrorResourceNotFound } = require('./server_error')

class WorkspaceServer {

  constructor (workspaceRepo, workspaceMemberRepo, router) {
    this.workspaceRepo = workspaceRepo
    this.workspaceMemberRepo = workspaceMemberRepo
    this.router = router
  }

  mount () {
    this.router.get('/', this.FindAll.bind(this))
    this.router.post('/', this.Save.bind(this))
    this.router.get('/:workspace', this.FindByUID.bind(this))
    this.router.put('/:workspace', this.Update.bind(this))
    this.router.delete('/:workspace', this.Remove.bind(this))

    return this.router
  }

  FindAll (req, res) {
    try {
      let workspaces = this.workspaceMemberRepo.FindAllByUserUID(req.session.user_uid)

      return res.json({
        data: workspaces.map(item => {
          return this.workspaceRepo.FindByUID(item.workspace_uid)
        })
      })
    } catch (error) {
      return res.status(400).json({ error })
    }
  }

  Save (req, res) {
    try {
      let workspace = Workspace.createWorkspace(req.body.name, req.body.slug)
      let workspaceMember = WorkspaceMember.createWorkspaceMember(req.session.user_uid, workspace.UID, 'admin')

      this.workspaceRepo.Save(workspace)
      this.workspaceMemberRepo.Save(workspaceMember)

      return res.json({ data: workspace })
    } catch (error) {
      return res.status(400).json({ error })
    }
  }

  FindByUID (req, res) {
    try {
      let workspace = this.workspaceRepo.FindByUID(req.params.workspace)

      // validate workspace ownership
      if (!this.workspaceMemberRepo.FindByUserUIDWithWorkspaceUID(workspace.UID, req.session.user_uid)) {
        throw Error(ServerErrorResourceNotFound)
      }
      return res.json({ data: workspace })
    } catch (error) {
      return res.status(400).json({ error })
    }
  }

  Update (req, res) {
    try {
      let workspace = this.workspaceRepo.FindByUID(req.params.workspace)

      // validate workspace ownership
      if (!this.workspaceMemberRepo.FindByUserUIDWithWorkspaceUID(workspace.UID, req.session.user_uid)) {
        throw Error(ServerErrorResourceNotFound)
      }

      workspace.changeName(req.body.name)

      this.workspaceRepo.Save(workspace)

      return res.json({ data: workspace })
    } catch (error) {
      return res.status(400).json({ error })
    }
  }

  Remove (req, res) {
    try {
      let workspace = this.workspaceRepo.FindByUID(req.params.workspace)

      // validate workspace ownership
      if (!this.workspaceMemberRepo.FindByUserUIDWithWorkspaceUID(workspace.UID, req.session.user_uid)) {
        throw Error(ServerErrorResourceNotFound)
      }

      this.workspaceMemberRepo.Remove(workspace)
      this.workspaceRepo.Remove(workspace)

      return res.json()
    } catch (error) {
      return res.status(400).json({ error })
    }

  }

}

module.exports = (workspaceRepo, workspaceMemberRepo) => {
  let router = express.Router()
  return (new WorkspaceServer(workspaceRepo, workspaceMemberRepo, router)).mount()
}
