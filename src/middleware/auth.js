module.exports = (req, res, next) => {
  if (!req.session.user_uid) {
    return res.status(400).json({ error: 'Unathorized' })
  }
  return next()
}
