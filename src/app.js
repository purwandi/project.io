const express = require('express')
const bodyParser = require('body-parser')
const compression = require('compression')
const logger = require('morgan')
const path = require('path')

require('dotenv').config()

module.exports = () => {
  const app =  new express()

  app.disable('x-powered-by')
  app.set('host', process.env.APP_HOST || '127.0.0.1')
  app.set('port', process.env.APP_PORT || 3000)
  app.set('app_url', 'http://' + app.get('host') + ':' + app.get('port'))

  if (process.env.NODE_ENV !== 'test') {
    app.use(logger('dev'))
  }

  app.use(compression())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(express.static(path.join(__dirname, '/public'), { maxAge: 3600 }))

  app.use(require('./middleware/cors'))
  app.use(require('./middleware/session'))

  return app
}
