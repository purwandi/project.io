const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('./../../app')()
const { UserRepositoryInMemory } = require('./../repository')
const { User } = require('./../domain')

let userRepo = UserRepositoryInMemory.init()

app.use('/auth', require('./auth_server')(userRepo))

chai.use(chaiHttp)
let agent = chai.request.agent(app)

describe('Auth http server test', () => {

  before (async () => {
    let user = await User.createUser('foobar', 'password')
    user.changeEmail('foo@bar.com')
    user.changeName('Foobar')

    userRepo.Save(user)

    await agent.post('/auth/signin')
      .send({
        username: 'foobar',
        password: 'password'
      })
  })

  after (() => {
    agent.close()
  })

  it('can login successfully', (done) => {
    chai.request(app)
      .post('/auth/signin')
      .send({
        username: 'foobar',
        password: 'password'
      })
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(res.body.data)
          .to.include({ username: 'foobar', email: 'foo@bar.com', name: 'Foobar' })
        done()
      })
  })

  it('can not login', (done) => {
    chai.request(app)
    .post('/auth/signin')
    .send({
      username: 'foobar2',
      password: 'password'
    })
    .end((err, res) => {
      chai.expect(res).to.have.status(400)
      done()
    })
  })

  it('can not logout', (done) => {
    chai.request(app)
      .get('/auth/signout')
      .end((err, res) => {
        chai.expect(res).to.have.status(400)
        done()
      })
  })

  it('can logout', (done) => {
    agent.get('/auth/signout')
      .end((err, res) => {
        chai.expect(res).to.have.status(200)
        done()
      })
  })

})
