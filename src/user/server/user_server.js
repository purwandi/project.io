const express = require('express')

class UserServer {

  constructor (userRepo, router) {
    this.userRepo = userRepo
    this.router = router
  }

  mount () {
    this.router.get('/', this.FindAll.bind(this))
    this.router.get('/:username',this.FindByUsername.bind(this))

    return this.router
  }

  FindAll (req, res) {
    try {
      let users = this.userRepo.FindAll()
      return res.json({ data: users })
    } catch (error) {
      return res.status(400).json({ error })
    }
  }

  FindByUsername (req, res) {
    try {
      let user = this.userRepo.FindByUsername(req.params.username)
      return res.json({ data: user })
    } catch (error) {
      return res.status(400).json({ error })
    }
  }

}

module.exports = (userRepo) => {
  let router = express.Router()
  return (new UserServer(userRepo, router)).mount()
}
