const chalk = require('chalk')
const initApp = require('./src/app')
const persistence = require('./src/persistence')

const app = initApp()

persistence().then(repo => {

  app.get('/', (req, res) => {
    console.log('Inside the homepage callback function')
    console.log(req.session.user_uid)
    res.send(`You hit home page!\n`)
  })

  app.use('/auth', require('./src/user/server/auth_server')(repo.userRepo))

  app.use('/me', require('./src/middleware/auth'), require('./src/user/server/profile_server')(repo.userRepo))
  app.use('/users', require('./src/middleware/auth'), require('./src/user/server/user_server')(repo.userRepo))
  app.use('/workspaces', require('./src/middleware/auth'), require('./src/workspace/server/')(repo))

  app.listen(app.get('port'), () => {
    console.log(
      '%s App is running at %s in %s mode',
      chalk.green('✓'), app.get('app_url'), app.get('env')
    )
    console.log('  Press CTRL-C to stop\n')
  })

}).catch(error => {
  console.log(error)
})
